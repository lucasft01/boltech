const express = require("express")
const cors = require("cors")
const bodyParser = require("body-parser")
const routes = require("./routes")
require('./config/database');
const { hostname, port } = require('./config/variables')

const app = express()

app.use(cors())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use("/", routes)

app.listen(port, hostname, () => console.log(`Api rodando em ${hostname}:${port}`))
