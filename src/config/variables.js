const dotenv = require("dotenv");
dotenv.config();

module.exports = {
    hostname: process.env.HOSTNAME || "localhost",
    port: process.env.PORT || 3000,
    db_name: process.env.DB_NAME,
    db_user: process.env.DB_USER,
    db_password: process.env.DB_PASSWORD,
    db_host: process.env.DB_HOST,
    db_port: process.env.DB_PORT || '27017',
    databaseURL: process.env.DATABASE_URI,
    authSecret: process.env.TOKEN || "JWT_TOKEN_SECRET",
};
