const mongoose = require('mongoose')
const { databaseURL, db_user, db_password, db_name, db_host, db_port } = require('../variables')

const uri = databaseURL || `mongodb://${db_user}:${db_password}@${db_host}:${db_port}/${db_name}?retryWrites=true&w=majority`
mongoose.Promise = global.Promise
mongoose.connect(
  uri,
  {
    useNewUrlParser: true,
    useCreateIndex: true
  },
  error => {
    if (error) {
      console.error(error)
    }
  }
)
