const { Router } = require("express")
const home = require("./home")
const account = require("./account")
const userMiddleware = require("../middlewares/userMiddleware")

const index = Router()
index.use("/home", home)
index.use("/account", userMiddleware, account)

module.exports = index
