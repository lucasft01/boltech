const { Router } = require("express")
const profile = require('./profile')
const project = require('./project')
const task = require('./task')

const index = Router()
index.use('/profile', profile)
index.use('/project', project)
index.use('/task', task)

module.exports = index