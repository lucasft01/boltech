const { Router } = require("express")
const {
  profile: {
    removeUser,
    updatePassword,
    updateUser
  }
} = require("../../controllers/account")

const profile = Router()
profile.route("/updatePassword").put(updatePassword)
profile.route("/removeUser").delete(removeUser)
profile.route("/updateUser").put(updateUser)

module.exports = profile
