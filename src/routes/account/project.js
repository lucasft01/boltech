const { Router } = require("express")
const {
  project: {
    createOneProject,
    deleteOneProject,
    findAllProject,
    findOneProject,
    updateOneProject
  }
} = require("../../controllers/account")

const project = Router()
project.route("/").get(findAllProject)
project.route("/").post(createOneProject)
project.route("/:projectId").get(findOneProject)
project.route("/:projectId").patch(updateOneProject)
project.route("/:projectId").delete(deleteOneProject)

module.exports = project
