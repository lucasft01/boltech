const { Router } = require("express")
const {
  task: {
    createOneTask,
    deleteItem,
    editItem,
    findAllTasks,
    finishItem
  }
} = require("../../controllers/account")

const task = Router()
task.route("/:projectId").get(findAllTasks)
task.route("/:projectId").post(createOneTask)
task.route("/:projectId/:taskId").patch(editItem)
task.route("/finish/:projectId/:taskId").patch(finishItem)
task.route("/:projectId/:taskId").delete(deleteItem)

module.exports = task
