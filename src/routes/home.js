const { Router } = require("express")
const { createUser, loginUser } = require("../controllers/home")
const home = Router()

home.route("/register").post(createUser)
home.route("/login").post(loginUser)

module.exports = home
