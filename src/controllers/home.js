const User = require('../models/User')

const createUser = async (req, res) => {
  try {
    const { userName, password, email } = req.body
    const user = new User({ userName, password, email })
    await user.createUser()
    await user.save()
    return res.send({ token: { value: user.refreshToken, isNew: true } }).status(200)
  } catch (e) {
    res.send(e).status(500)
  }
}

const loginUser = async (req, res) => {
  try {
    const { userName, password } = req.body
    const user = await User.findOne({ userName })
    if (user) {
      const newRefreshToken = await user.login(userName, password)
      if (newRefreshToken) {
        await user.save()
        return res.send({ token: { value: newRefreshToken, isNew: true } }).status(200)
      }
      return res.sendStatus(406)
    }
    return res.sendStatus(404)
  } catch (e) {
    res.send(e).status(500)
  }
}

module.exports = { createUser, loginUser }