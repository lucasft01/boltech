const User = require('../../models/User')

const updateUser = async (req, res) => {
    try {
        const { body, token, userName } = req
        const { newUserName, password, email } = body
        const user = await User.findOne({ refreshToken: token.value })
        const isValid = await user.validateUser(userName, password)
        if (!isValid) return res.send({ token }).status(406)
        if ((newUserName.trim() === null || newUserName.trim() === "") || (email.trim() === null || email.trim() === ""))
            return res.send({ token }).status(406)
        await user.updateOne({ userName: newUserName, email })
        const newRefreshToken = await user.generateRefreshToken()
        await user.save()
        await Domain.updateMany({ userName }, { userName: newUserName })
        return res.send({ token: { value: newRefreshToken, isNew: true } }).status(200)
    } catch (e) {
        console.error(e)
        return res.send({ token: req.token, e }).status(500)
    }
}

const updatePassword = async (req, res) => {
    try {
        const { body, token, userName } = req
        const { oldPassword, newPassword, repeatPassword } = body
        if (newPassword !== repeatPassword) return res.send({ token }).status(406)
        const user = await User.findOne({ refreshToken: token.value })
        const isValid = await user.validateUser(userName, oldPassword)
        if (!isValid) return res.send({ token }).status(406)
        const encryptPassword = await user.encryptPassword(newPassword)
        const newRefreshToken = await user.generateRefreshToken()
        await user.updateOne({ password: encryptPassword })
        await user.save()
        return res.send({ token: { value: newRefreshToken, isNew: true } }).status(200)
    } catch (e) {
        return res.send({ token: req.token, e }).status(500)
    }
}

const removeUser = async (req, res) => {
    try {
        const { token, userName } = req
        const user = await User.findOne({ refreshToken: token.value, userName })
        await user.remove()
        const domain = await Domain.find({ userName })
        domain.forEach(
            async function (doc) {
                await Manifest.deleteMany({ domain: doc.url })
                await doc.remove()
                doc.save()
            }
        )
        await user.save()
        return res.send({ token }).status(200)
    } catch (e) {
        return res.send({ token: req.token, e }).status(500)
    }
}

module.exports = { updateUser, updatePassword, removeUser }