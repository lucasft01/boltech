const profile = require('./profile')
const project = require('./project')
const task = require('./task')

module.exports = { profile, project, task }